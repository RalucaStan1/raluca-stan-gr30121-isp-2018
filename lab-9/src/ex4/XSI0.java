package ex4;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class XSI0 extends JFrame {

    private JButton[][] buttons;
    private int[][] state;
    private int turn;
    private int winner;

    XSI0() {
        setTitle("X si 0");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(600, 600);
        setVisible(true);
        init();


    }

    private void init() {
        buttons = new JButton[3][3];
        state = new int[3][3];
        winner = 0;

        placeButtons();
    }

    private void placeButtons() {
        int x = 10;
        int y = 10;
        for (int i=0; i < 3; i++) {
            for (int j=0; j < 3; j++) {
                buttons[i][j] = new JButton();
                state[i][j] = 0;
                buttons[i][j].setBounds(x + 100*i, y + 100 * j, 70, 70);
                buttons[i][j].addActionListener(new ClickListener(i, j));
                add(buttons[i][j]);
            }
        }
    }

    private void checkWinner() {
        if (winner != 0) {
            return;
        }

        if (state[0][0] == state[0][1] && state[0][1] == state[0][2]) {
            winner = state[0][0];
        }

        if (state[1][0] == state[1][1] && state[1][1] == state[1][2]) {
            winner = state[1][0];
        }

        if (state[2][0] == state[2][1] && state[2][1] == state[2][2]) {
            winner = state[2][0];
        }

        if (state[0][0] == state[1][0] && state[1][0] == state[2][0]) {
            winner = state[0][0];
        }

        if (state[0][1] == state[1][1] && state[1][1] == state[2][1]) {
            winner = state[0][1];
        }

        if (state[0][2] == state[1][2] && state[1][2] == state[2][2]) {
            winner = state[0][2];
        }

        if (state[0][0] == state[1][1] && state[1][1] == state[2][2]) {
            winner = state[0][0];
        }

        if (state[0][2] == state[1][1] && state[1][1] == state[2][0]) {
            winner = state[0][2];
        }

        if (winner != 0) {
            System.out.println("Winner is " + (winner == 1 ? "X" : "0"));
        }
    }

    public static void main(String[] args) {
        new XSI0();
    }

    private class ClickListener implements ActionListener {

        private int row;
        private int col;

        ClickListener(int row, int col) {
            this.row = row;
            this.col = col;
        }

        @Override
        public void actionPerformed(ActionEvent e) {
            if (buttons[row][col].getText().isEmpty()) {
                buttons[row][col].setText(turn % 2 == 0 ? "X" : "0");
                state[row][col] = turn % 2 == 0 ? 1 : 2;
                turn = (turn + 1) % 2;
            }

            checkWinner();

        }
    }

}
