package ex3;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.*;

public class  Content  extends JFrame {


    JTextField t;
    JButton b;
    JLabel l;
    JTextArea area;

    Content() {
        setTitle("Click here");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 100;
        int height = 60;

        l = new JLabel("Content file");
        l.setBounds(20, 100, width, height);

        t = new JTextField();
        t.setBounds(80, 120, width, height);
        t.setSize(80, 80);

        b = new JButton("Display");
        b.setBounds(100, 40, width, height);
        b.setSize(80, 30);
        b.addActionListener(new e());

        area= new JTextArea();
        area.setBounds(100, 200,width,height);


        add(l);
        add(t);
        add(b);
        add(area);
    }


    public static void main(String[] args) {
        new Content();
    }

    class e implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            area.setText("");
            try {
                Scanner in=new Scanner(new File(t.getText()));
                while(in.hasNextLine()){
                    area.append(in.nextLine());
                }

            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }




        }
    }


}


