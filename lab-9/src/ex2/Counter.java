package ex2;
import javax.swing.JFrame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;



public class  Counter  extends JFrame {


    JTextField t;
    JButton b;
    JLabel l;

    Counter() {
        setTitle("Click here");
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        init();
        setSize(300, 300);
        setVisible(true);
    }

    public void init() {
        this.setLayout(null);
        int width = 100;
        int height = 60;

        l = new JLabel("Counter");
        l.setBounds(20, 100, width, height);

        t = new JTextField("0");
        t.setBounds(80, 120, width, height);
        t.setSize(80, 80);

        b = new JButton("Increment");
        b.setBounds(100, 140, width, height);
        b.setSize(80, 30);
        b.addActionListener(new e());

        add(l);
        add(t);
        add(b);
    }


    public static void main(String[] args) {
        new Counter();
    }

    class e implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {

            int n = Integer.parseInt(Counter.this.t.getText());
            n++;
            Counter.this.t.setText(String.valueOf(n));
        }
    }


}

