package ex3;

import ex2.Bank;

public class TestBankAccount {
    public static void main(String[] args) {
        ex2.Bank bank = new Bank();
        bank.addAccount("Rusu Ruxandra", 200.0);
        bank.addAccount("Andrei Andreea", 1200.0);
        bank.addAccount("Guciur Alex", 400.0);
        bank.addAccount("Stan Lucian",80.0);
        System.out.println("Print Accounts by balance");
        bank.printAccounts();
        System.out.println("Print Accounts between limits");
        bank.printAccounts(60.0,100.0);
        System.out.println("Get Account by owner name");
        bank.getAccount("Rusu Ruxandra");
        System.out.println("Get All Account order by name");
        bank.getAllAccounts();
    }
}
