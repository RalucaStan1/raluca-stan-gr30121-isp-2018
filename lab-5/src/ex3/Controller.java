package ex3;

import java.util.Timer;

public class Controller {

    public Sensor lightSensor;
    public Sensor temperatureSensor;
    public Controller(){
        lightSensor=new LightSensor();
        temperatureSensor=new TemperatureSensor();
    }

    public void control(){

        for(int i=0; i<20; i++){
            long time = System.currentTimeMillis();
            while (System.currentTimeMillis() - time < 1000);
            System.out.println("Iteration: " + i);
            System.out.println("Light sensor: " + lightSensor.readValue());
            System.out.println("Temp sensor: " + temperatureSensor.readValue());
        }
    }
}
