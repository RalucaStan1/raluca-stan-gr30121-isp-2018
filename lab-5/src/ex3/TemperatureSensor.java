package ex3;

import java.util.Random;

public class TemperatureSensor extends Sensor {


    @Override
    public int readValue() {
        Random r=new Random();
        int sol = r.nextInt();
        return (sol > 0) ? sol % 101 : -sol % 101;

    }
}
