package ex2;

public class Test {
    public static void main(String[] args) {

        RealImage r1=new RealImage("Folder1");
        r1.RotatedImage();
        r1.display();
        ProxyImage p1=new ProxyImage("Folder2");
        p1.display();
        p1.RotatedImage();
    }
}
