package ex4;

import ex3.LightSensor;
import ex3.Sensor;
import ex3.TemperatureSensor;

public class Controller {

    public Sensor lightSensor;
    public Sensor temperatureSensor;
    private static Controller controller;

    private Controller(){
        lightSensor=new LightSensor();
        temperatureSensor=new TemperatureSensor();
    }

    public static Controller getInstance() {
        if (controller == null) {
            controller = new Controller();
        }
        return controller;
    }

    public void control(){

        for(int i=0; i<20; i++){
            long time = System.currentTimeMillis();
            while (System.currentTimeMillis() - time < 1000);
            System.out.println("Iteration: " + i);
            System.out.println("Light sensor: " + lightSensor.readValue());
            System.out.println("Temp sensor: " + temperatureSensor.readValue());
        }
    }
}
