package ex4;

public class Test {

    public static void main(String[] args) {

        Controller c1 = Controller.getInstance();
        Controller c2 = Controller.getInstance();

        System.out.println("C1 == C2: " + (c1 == c2 ));
        c1.control();
    }
}
