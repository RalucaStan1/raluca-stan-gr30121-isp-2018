package ex1;

public class Test {
    public static void main(String[] args) {


        Shape [] s1=new Shape[3];
        s1[0]=new Circle(2.0,"green",true);
        s1[1]=new Rectangle(5.0,6.5,"black",false);
        s1[2]=new Square(3.0,"black",true);

        System.out.println(s1[0].getArea());
        System.out.println(s1[1].getArea());
        System.out.println(s1[2].getArea());
        System.out.println(s1[0].getPerimeter());
        System.out.println(s1[1].getPerimeter());
        System.out.println(s1[2].getPerimeter());
    }
}
