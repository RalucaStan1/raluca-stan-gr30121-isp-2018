package ex6;

public class Test {
    public static void main(String[] args){
        Shape s1=new Shape();
        Shape s2=new Shape("red",true);

        s1.tooString();
        s2.tooString();

        Circle c1=new Circle();
        Circle c2=new Circle(8.9);
        Circle c3=new Circle(3.4,"green",true);

        c1.toooString();
        c2.toooString();
        c3.toooString();

        Rectangle r1=new Rectangle();
        Rectangle r2=new Rectangle(9,8);
        Rectangle r3=new Rectangle(14,7,"pink",false);

        r1.toStringR();
        r2.toStringR();
        r3.toStringR();
        Square sq1=new Square();
        Square sq2=new Square(10);
        Square sq3=new Square(12,"black",false);

        sq1.toStringS();
        sq2.toStringS();
        sq3.toStringS();
    }
}

