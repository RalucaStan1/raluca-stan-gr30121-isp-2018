package ex6;

public class Rectangle extends Shape {
    double width=1.0,length=1.0;

    Rectangle(){
        this.width=1.0;
    }

    Rectangle(double width,double length){
        this.width=width;
        this.length=length;
    }

    Rectangle(double width,double length,String color,boolean filled){
        super(color,filled);
        this.width=width;
        this.length=length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea(){
        return this.width*this.length;
    }

    public double getPerimeter(){
        return 2*this.width+2*this.length;
    }

    public void toStringR(){
        System.out.println("A Rectangle with width= "+this.width+" and length= "+this.length+" , which is a subclass of ");
        super.tooString();
    }
}
