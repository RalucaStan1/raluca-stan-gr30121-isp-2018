package ex6;

public class Square extends Rectangle {
    Square(){
    }

    Square(double side){
        super(side,side);
    }

    Square(double side,String color,boolean filled){
        super(side,side,color,filled);
    }

    public double getSide(){
        return this.getLength();
    }

    public void setSide(double side){
        this.setLength(side);
        this.setWidth(side);
    }

    public void setWidth(double side){
        super.setWidth(side);
    }

    public void setLength(double side){
        super.setLength(side);
    }

    public void toStringS(){
        System.out.println("A Square with side="+this.getSide()+" , which is a subclass of ");
        super.toStringR();
    }
}
