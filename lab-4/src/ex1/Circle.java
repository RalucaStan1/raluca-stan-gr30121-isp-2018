package ex1;

public class Circle {
    private double radius;
    private String color;

    public  Circle(){
        this.radius=1.0;
        this.color="red";
    }

    public  Circle(double r,String c){
        this.radius=r;
        this.color=c;
    }



    public double getRadius(){
       return this.radius;

    }

    public double getArea(){
        return Math.PI*this.radius*this.radius;

    }

    public void getColor(){
        System.out.println("Get color: "+ this.color);
    }



}
