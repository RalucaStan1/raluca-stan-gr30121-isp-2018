package ex1;

public class TestCircle {

    public static void main(String[] args) {

        Circle c1=new Circle(14.0, "green");

        System.out.println("c1");
        c1.getColor();
        System.out.print(c1.getRadius());
        System.out.println(c1.getArea());

        Circle c2=new Circle(16.0, "blue");

        System.out.println("c2");
        c2.getColor();
        System.out.print(c2.getRadius());
        System.out.print(c2.getArea());
    }
}
