package ex3;

public class Author {

     String name;
     String email;
     char gender;

    public Author(){
        this.name="Agatha Kristi";
        this.email="agathakristi@yahoo.com";
        this.gender='f';
    }



    public String getName() {
        return name;
    }

    public char getGender() {
        return gender;
    }


    public String getEmail() {
        return email;
    }

    public String toString() {
        return this.name + "(" + this.gender + ") at " + this.email;
    }

}
