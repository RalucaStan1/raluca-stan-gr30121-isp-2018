package ex4;


public class Book {
    String name;
    Author[] authors;
    double price;
    int qtyInStock = 0;
    private int i;

    public Book(String name, Author[] authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, Author[] authors, double price, int qtyInStock) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }


    public String toString() {
        String s = this.name + " by ";
        int i;
        for (i = 0; i < authors.length; i++) {
            s = s + this.authors[i].getName() + " ( " + this.authors[i].getGender() + " ) at email:" + this.authors[i].getEmail();
        }
        return s;
    }

    public void printAuthors() {
        int i;
        for (i = 0; i < authors.length; i++) {
            System.out.println(this.authors[i].getName());
        }
    }
}

