package ex4;

public class TestAuthor {
    public static void main(String[] args) {
        int i;
        Author[] a1 = new Author[4];
        Author[] a2 = new Author[3];
        a1[0] = new Author("Pasc", "pasc.vlad@yahoo", 'm');
        a1[1] = new Author("Gheorghe", "gheorghita@yahoo", 'm');
        a1[2] = new Author("Mira", "miro@yahoo", 'f');
        a1[3] = new Author("Florica", "flori@yahoo", 'f');

        a2[0] = new Author("Vlad", "pasc@yahoo", 'm');
        a2[1] = new Author("Iona", "io@yahoo", 'f');
        a2[2] = new Author("Luci", "luci@yahoo", 'm');
        Book b1 = new Book("Muntii Ceahlau", a1, 70.0);
        Book b2 = new Book("Padurea Neagra", a2, 50.0, 2);
        b1.setQtyInStock(4);
        System.out.println(b2.getQtyInStock());
        System.out.println(b1.getQtyInStock());
        for (i = 0; i < 4; i++) {
            System.out.println(b2.getAuthors());
        }
        b1.printAuthors();
    }
}
