package ex4;

import java.util.Random;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;

public class TestRobot {
    public static void main(String[] args) {

        int x;
        int y;
        Scanner in=new Scanner(System.in);
        x=in.nextInt();
        y=in.nextInt();
        int[][] a=new int[100][100];
        Robot [] r=new Robot[10];
        for(int i=0; i<10;i++){

            Random rr=new Random();

            r[i]=new Robot(i+1,rr.nextInt(x), rr.nextInt(y));
        }

        int alive=10;

            Timer t = new Timer();
            t.scheduleAtFixedRate(new Task(x, y, r, a), 100, 100);




    }

    private static class Task extends TimerTask {

        private int x;
        private int y;
        Robot [] r;
        int [][] a;

        Task(int x, int y, Robot r[], int [][] a) {
            this.x = x;
            this.y = y;
            this.r=r;
            this.a=a;
        }

        @Override
        public void run() {

            Random rr=new Random();
            for(int i=0;i<10;i++) {

                if (r[i].isiAlive()) {

                    a[r[i].getX()][r[i].getY()]=0;
                    r[i].setX(rr.nextInt(x));
                    r[i].setY(rr.nextInt(y));
                    if(a[r[i].getX()][r[i].getY()]==0){

                        a[r[i].getX()][r[i].getY()]= (int) r[i].getId();
                    }
                    else {

                        Robot robot=r[a[r[i].getX()][r[i].getY()]-1];
                        r[i].setAlive(false);
                        robot.setAlive(false);
                        a[r[i].getX()][r[i].getY()]=0;
                    }
                }


            }

            for (int i=0; i < x; i++) {
                for (int j=0; j < y; j++) {
                    System.out.print(a[i][j]);
                    System.out.print(' ');
                }
                System.out.println();
            }

        }



    }
}
