import java.io.*;

public class Pb {

    public static void main(String[] args) throws IOException {
        BufferedReader br=new BufferedReader(new FileReader("intrare.txt"));
        String[] words = br.readLine().split(" ");
        BufferedWriter bw = new BufferedWriter(new FileWriter("iesire.txt"));
        for (String word : words) {
            bw.append(word);
            bw.newLine();
        }

        br.close();
        bw.close();
    }
}
