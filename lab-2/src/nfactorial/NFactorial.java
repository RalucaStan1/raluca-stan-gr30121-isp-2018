package nfactorial;

import java.util.Scanner;

public class NFactorial {

    public static void main(String[] args) {

        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        System.out.println("Iterativ " + iterativFactorial(n));
        System.out.println("Recursiv " + recursivFactorial(n));

    }

    public static int iterativFactorial ( int n){

        int factorial=1;
        for(int i=2; i<=n; i++){

            factorial=factorial*i;

        }
        return factorial;
    }

    public static int recursivFactorial(int n){

        if(n==1) return 1;
        return n*recursivFactorial(n-1);
    }
}
