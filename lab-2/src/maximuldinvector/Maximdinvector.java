package maximuldinvector;

import java.util.Scanner;

public class Maximdinvector {

    public static void main(String[] args) {

        Scanner in=new Scanner(System.in);
        int n=in.nextInt();
        int[] x = new int[n];

        for(int i=0; i<n; i++){
            x[i]=in.nextInt();
        }
        int max=x[0];
        for(int i=1; i<n; i++){
            if(x[i]>max)
                max=x[i];

        }
        System.out.println("Maximul este " + max);

    }
}
