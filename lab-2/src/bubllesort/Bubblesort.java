package bubllesort;

import java.util.Random;
import java.util.Scanner;

public class Bubblesort {

    public static void main(String[] args) {

        Scanner in=new Scanner(System.in);
        int N=10;
        Random r=new Random();
        int [] x=new int[N];
        for(int i=0; i<N; i++){
            x[i]=r.nextInt();

        }
        System.out.print("Sirul este " );
        for(int i=0; i<N; i++){
            System.out.print(x[i] + ", ");
        }
        System.out.println();
        BubbleSort(x);
        System.out.print("Sirul sortat este ");
        for(int i=0; i<N;i++){
            System.out.print( x[i] + ", ");
        }

    }

    private static void BubbleSort(int arr[])
    {
        int n = arr.length;
        for (int i = 0; i < n-1; i++)
            for (int j = 0; j < n-i-1; j++)
                if (arr[j] > arr[j+1])
                {

                    int temp = arr[j];
                    arr[j] = arr[j+1];
                    arr[j+1] = temp;
                }
    }
}
