package NumerePrime;

import java.util.Scanner;

public class NumerePrime {

    public static boolean ePrim(int a) {
        for(int d=2; d<a; d++) {
            if (a%d == 0)
                return false;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);

        System.out.print("Enter an integer");

        int a = in.nextInt();
        System.out.println("You entered integer " + a);

        System.out.print("Enter an integer");
        int b = in.nextInt();
        System.out.println("You entered integer " + b);

        int nr=0;
        for (int i=a; i<=b; i++) {
            if (ePrim(i) == true) {
                System.out.println(i);
                nr++;
            }
        }
        System.out.println("Nr = " + nr);
    }
}
