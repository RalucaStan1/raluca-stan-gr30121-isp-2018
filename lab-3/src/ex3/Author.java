package ex3;

public class Author {

    private String name;
    private String email;
    private char gendre;

    public Author(String n, String e, char g){
        this.name=n;
        this.email=e;
        this.gendre=g;
    }

    public String getName() {
        return name;
    }

    public char getGender() {
        return gendre;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void tString() {
        System.out.println(this.name + "(" + this.gendre + ") at " + this.email);
    }
}
