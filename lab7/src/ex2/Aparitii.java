package ex2;

import java.io.*;
import java.util.Scanner;

public class Aparitii {

    public static void main(String[] args) {
        try {
            char e = args[0].charAt(0);
            Reader reader = new BufferedReader(new InputStreamReader(
                    new FileInputStream("data.txt")));
            int aparitii = 0;
            while (reader.ready()) {
                char c = (char) reader.read();
                if (e == c) {
                    aparitii++;
                }
            }
            System.out.println("Aparitii: " + aparitii);
        } catch (IOException e1) {
//            e1.printStackTrace();
            System.out.println("Eroare citire fisier!");
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Specifica argumentul e");
        }
        System.out.println("Gata executia");


    }
}
