package ex1;

public class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        System.out.println(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}
