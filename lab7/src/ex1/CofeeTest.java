package ex1;

public class CofeeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();



        for (int i = 0; i < 15; i++) {
            try {
                Cofee c = mk.makeCofee();

                try {
                    d.drinkCofee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:" +  " temp=" + e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:" + " conc=" + e.getConc());
                } finally {
                    System.out.println("Throw the cofee cup.\n");
                }


            } catch (MyException e) {
                System.out.println("Exception: Over 15 cofees ");
            }
        }
    }
}
