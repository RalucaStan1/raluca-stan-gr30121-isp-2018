package ex1;

public class ConcentrationException extends Exception {
    int c;
    public ConcentrationException(int c,String msg) {
        System.out.println(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}
