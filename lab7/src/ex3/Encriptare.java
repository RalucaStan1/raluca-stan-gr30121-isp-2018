package ex3;

import java.io.*;

public class Encriptare {

    private static void encrypt(String numeFisier) throws IOException {
        Reader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(numeFisier)));
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(numeFisier + ".enc")));
        while (reader.ready()) {
            int c = reader.read();
            writer.write(c << 1);
        }
        writer.flush();
        writer.close();
        reader.close();
    }

    private static void decrypt(String numeFisier) throws IOException {
        Reader reader = new BufferedReader(new InputStreamReader(
                new FileInputStream(numeFisier)));
        Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(numeFisier + ".dec")));
        while (reader.ready()) {
            int c = reader.read();
            writer.write(c >> 1);
        }
        writer.flush();
        writer.close();
        reader.close();
    }

    public static void main(String[] args) {
        try {
            String op = args[0];
            String numeFisier = args[1];

            switch(op) {
                case "encrypt": encrypt(numeFisier); break;
                case "decrypt": decrypt(numeFisier); break;
                default: System.out.println("Operatie invalida");
            }

        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Te rog specifica operatia si fisierul");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
