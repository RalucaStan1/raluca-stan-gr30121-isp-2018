package ex4;

import java.io.Serializable;

public class Car implements Serializable {

    private String model;
    private float price;

    public Car(String model,float price){
        this.model=model;
        this.price=price;

    }

    public String getModel() {
        return model;
    }

    public float getPrice() {
        return price;
    }
}
