package ex4;

import java.io.*;

public class Browser {

    private static void createCar(String model, float price) throws IOException {
        Car car = new Car(model, price);
        File folder = new File("Cars/" + model);
        folder.createNewFile();
        ObjectOutput out = new ObjectOutputStream(new FileOutputStream(folder));
        out.writeObject(car);
        out.close();
    }

    private static void viewSpecific(String model) throws IOException, ClassNotFoundException {
        ObjectInput in = new ObjectInputStream(new FileInputStream("Cars/" + model));
        Car car = (Car) in.readObject();
        System.out.println("Car model: " + car.getModel());
        System.out.println("Car price: " + car.getPrice());
        in.close();
    }

    private static void viewAll() {
        File folder = new File("Cars");
        String[] files = folder.list();

        System.out.println("Available models:");
        for (int i=0; i < files.length; i++) {
            System.out.println(files[i]);
        }
    }

    public static void main(String[] args) {
        try {
            String op = args[0];
            switch (op) {
                case "create": {
                    String model = args[1];
                    float price = Float.parseFloat(args[2]);
                    createCar(model, price);
                } break;
                case "view": viewAll(); break;
                case "specific": {
                    String model = args[1];
                    viewSpecific(model);
                } break;
            }
        } catch (IOException e) {
//            e.printStackTrace();
            System.out.println("Nu exista modelul");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (ArrayIndexOutOfBoundsException e) {
            System.out.println("Specifica argumentele");
        }

    }
}
